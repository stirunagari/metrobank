import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router'
import '@/css/main.scss';
import store from '@/store'

import axios from 'axios';
import VueAxios from 'vue-axios';
import { ValidationProvider } from 'vee-validate';
import { ValidationObserver } from 'vee-validate';

Vue.config.productionTip = false

Vue.prototype.$http = axios;
Vue.use(VueAxios, axios);

// Register it globally
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

// axios.interceptors.request.use(function (config) { 
// 	if( config.method == 'get' ){ 
// 		config.url+='&_'+(new Date().getTime()) 
// 	} 
// 	return config; 
// }, function (error) { 
//  	return Promise.reject(error); 
// });

// router.beforeEach((to, from, next) => {
//   document.title = to.meta.title;
//   next()
// })

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

// new Vue({
//   el: '#app',
//   router,
//   template: '<App/>',
//   components: { App }
// })
