import Vue from 'vue'
import VueRouter from 'vue-router'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import NavBar from './components/NavBar.vue'

import Home from './views/Home.vue'
import Login from './views/Login.vue'

import Admin from './views/Admin.vue'
import AdminScreen from './views/AdminScreen.vue'
import AdminGroup from './views/AdminGroup.vue'

Vue.use(VueRouter)
Vue.use(BootstrapVue);

Vue.component('side-nav', NavBar);

const routes = [
  { path: '*', redirect: '/'},
  {
    name: 'home',
    path: '/',
    component: Home,
    props: true
  },{
    name: 'login',
    path: '/login',
    component: Login,
    props: true
  },{
    name: 'admin',
    path: '/admin',
    component: Admin,
  },{
    name: 'admin-screen',
    path: '/admin/screen/:name',
    component: AdminScreen,
    props: true
  },{
    name: 'admin-group',
    path: '/admin/group/:name',
    component: AdminGroup,
    props: true
  }
]

const router = new VueRouter({
  routes
})

export default router
