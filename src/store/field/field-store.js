export const state = {
    status : null
}

export const getters = {
    getStatus (state){
        return state.status
    }
}

export const mutations = {
    setStatus( state, form_status){
        state.status = form_status
    }
}

export const actions = {
    updateStatus (state, form_status){
        state.commit('setStatus', form_status);
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}