import Vue from 'vue'
import Vuex from 'vuex'
import FieldStore from './field/field-store.js'

Vue.use(Vuex)

export default new Vuex.Store({
    modules : {
        FieldStore
    }
})