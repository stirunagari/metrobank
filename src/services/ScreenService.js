import axios from 'axios';

export const screenService = {
    host: 'http://localhost:4000/api',

    getFullDetail : function(name){
        return axios.get(
            this.host+'/screen/fulldetail/'+name, {}, {}
        ).then(function(response){
            return response.data;
        })
    }
}