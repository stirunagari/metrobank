import axios from 'axios';

export const crudService = {
    host: 'http://localhost:4000/api',

    add : function(url, obj){
        obj['created_date'] = new Date();
        obj['deleted'] = false;
        return axios.post(
            this.host+url, obj, {}
        ).then(function(response){
            return response.data;
        })
    },
    getAll : function(url){
        return axios.get(
            this.host+url, {}, {}
        ).then(function(response){
            return response.data;
        })
    },
    getDetails : function(url){
        return axios.get(
            this.host+url, {}, {}
        ).then(function(response){
            return response.data;
        })
    },
    getById : function(url){
        return axios.get(
            this.host+url, {}, {}
        ).then(function(response){
            return response.data;
        })
    },
    update : function(url, obj){
        return axios.put(
            this.host+url, obj, {}
        ).then(function(response){
            return response.data;
        })
    }
}