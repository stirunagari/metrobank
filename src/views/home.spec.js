import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import { ValidationProvider } from 'vee-validate'
import Home from '@/views/Home.vue'
import Screen from '@/components/Screen.vue'
import Group from '@/components/Group.vue'
import Field from '@/components/Field.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Home page unit tests', () => {
    let screen
    let wrapper

    beforeEach(() => {
        wrapper = shallowMount(Home, {
            propsData: {
                screen: screen
            },
            components: {
                ValidationProvider
            }
            // ,
            // mocks: {
            //     $store: new Vuex.Store({
            //         state: {},
            //         mutations
            //     })
            // }
        })
    })
    it('Should have a screen component', () => {
        expect(Screen.exists()).toBe(true)
    })
})