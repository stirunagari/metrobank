const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let field = new Schema({
    name : {
        type: String
    },
    label : {
        type: String
    },
    type : {
        type: String
    },
    required : {
        type: Boolean
    },
    group : {
        type: Schema.Types.ObjectId,
        ref: "group"
    },
    city : {
        type: Schema.Types.ObjectId,
        ref: "city"
    },
    created_date : {
        type: Date
    },
    deleted : {
        type: Boolean
    }
},{
    collection: 'fields'
})

module.exports = mongoose.model('field', field)