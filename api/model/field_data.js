const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let field_data = new Schema({
    label : {
        type: String
    },
    value : {
        type: String
    },
    field : {
        type: Schema.Types.ObjectId,
        ref: "group"
    },
},{
    collection: 'fields_data'
})

module.exports = mongoose.model('field_data', field_data)