const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let group = new Schema({
    label : {
        type: String
    },
    name : {
        type: String
    },
    city : {
        type: Schema.Types.ObjectId,
        ref: "city"
    },
    screen : {
        type: Schema.Types.ObjectId,
        ref: "screen"
    },
    created_date : {
        type: Date
    },
    deleted : {
        type: Boolean
    }
},{
    collection: 'groups'
})

module.exports = mongoose.model('group', group)