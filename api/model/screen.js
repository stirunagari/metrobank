const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let screen = new Schema({
    label : {
        type: String
    },
    name : {
        type: String
    },
    sub_title : {
        type: String
    },
    description : {
        type: String
    },
    city : {
        type: Schema.Types.ObjectId,
        ref: "city"
    },
    created_date : {
        type: Date
    },
    deleted : {
        type: Boolean
    }
},{
    collection: 'screens'
})

module.exports = mongoose.model('screen', screen)