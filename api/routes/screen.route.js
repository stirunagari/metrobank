const express = require('express');
const screen_route = express.Router();

let crud = require('../controller/crud.controller');
let screen_controller = require('../controller/screen.controller');

// screen model
let screen = require('../model/screen');

// Add screen
screen_route.route('/screen/add').post((req, res, next) => {
  crud.addRecord(screen, req, res, next);
});

// Get all screen
screen_route.route('/screens').get((req, res, next) => {
  screen_controller.getAll(res, next);
})

// Get screen by id
screen_route.route('/screen/:id').get((req, res, next) => {
  crud.getById(screen, req.params.id, res, next);
})

// Get screen by name
screen_route.route('/screen/detail/:name').get((req, res, next) => {
  screen_controller.getDetailsByName(req.params.name, res, next);
})

// Get screen by name
screen_route.route('/screen/fulldetail/:name').get((req, res, next) => {
  screen_controller.getFullDetails(req.params.name, res, next);
})

// Update screen
screen_route.route('/screen/:id').put((req, res, next) => {
  crud.update(screen, req, res, next);
})

// Delete screen
screen_route.route('/screen/:id').delete((req, res, next) => {
  crud.delete(screen, req, res, next);
})

module.exports = screen_route;