var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/metrobank";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("mydb");
  var myquery = { _id: '5ea57d67ecd0a6789310d853' };
  dbo.collection("group").deleteOne(myquery, function(err, obj) {
    if (err) throw err;
    console.log("1 document deleted");
    db.close();
  });
});