let screen = require('../model/screen');
let group = require('../model/group');
let field = require('../model/field');
let field_data = require('../model/field_data');

module.exports = {
    //Get all screens
    getAll : (response , next) => {
      screen.find()
        .populate("city")
        .then(function(data) {
          response.json(data);
        })
        .catch(function(error) {
          return next(error);
        });
    },
    //Get details by name
    getDetailsByName : (screen_name, response, next) => {
      var result = {'screen' : {}}
      screen.find({name : screen_name})
        .populate("city")
        .then(function(data) {
          result['screen'] = data[0]
          // response.json(data);
          return group.find({screen : data[0]._id})
        })
        .then(function(data){
          result['groups'] = data
          response.json(result)
        })
        .catch(function(error) {
          return next(error);
        });
    },

    //Get full details by name for UI
    getFullDetails : async (screen_name, response, next) => {
        let result = {'screen' : {}}
        let screens = await screen.find({name : screen_name})
        if(screens.length == 1 ){
          result.screen = screens[0].toJSON();
          let groups = await group.find({screen: screens[0]._id})
          for(let i = 0; i < groups.length; i++ ){
            groups[i] = groups[i].toJSON();
            let fields = await field.find({group : groups[i]._id});
            for(let j = 0;j<fields.length;j++){
                fields[j] = fields[j].toJSON();
                if(fields[i].type == 'radio' || fields[i].type == 'select'){
                    fields[i]['options'] = await field_data.find({field : fields[i]._id}); 
                }
            }
            groups[i]['fields'] = fields
          }
          result.screen['groups'] = groups;
        }
        response.json(result.screen);
    }
}