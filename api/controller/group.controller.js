let group = require('../model/group');
let field = require('../model/field');

module.exports = {
    
    //Get details by name
    getDetailsByName : (group_name, response, next) => {
      var result = {'group' : {}}
      group.find({name : group_name})
        .populate("screen")
        .populate("city")
        .then(function(data) {
          result['group'] = data[0]
          // response.json(data);
          return field.find({group : data[0]._id})
        })
        .then(function(data){
          result['fields'] = data
          response.json(result)
        })
        .catch(function(error) {
          return next(error);
        });
    }
}