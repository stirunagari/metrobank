let field = require('../model/field');
let field_data = require('../model/field_data');
const { response } = require('express');

module.exports = {
    
    //Add 
    addRecord : (request, response, next) => {
        var options = request.body.options
        field.create(request.body, (error, data) => {
            if(error){
                return next(error)
            }
            if(options.length > 0){
                for(let i=0;i<options.length;i++){
                    options[i]['field'] = data._id
                }
                return field_data.create(options)
            }
        })
        .then((error, data)=>{
            if(error){
                return next(error)
            }
            console.log(data)
            response.json(data)
        })
    },
}